/*
  * Copyright (c) 24-25, Swedish Institute of Computer Science.
  * ��� ����� ���������������. *
  * ��������� ���������������, ������������� � �������� � �������� ����,
  * � ������������ ��� ��� - �����������, ���� ����������� ���������
  * �������:
  * 1. ��������������� ��������� ���� ������ ��������� ������������� �������
  *    ���������, ���� ������ ������� � ��������� �������� ��������.
  * 2. ��������������� ��������� ���� ������ ��������� ������������� �������
  *    ���������, ���� ������ ������� � ��������� �������� �������� �
  *    ������������ �/��� ������ ����������, ������� ����� �������������
  *    ������ � ����������������� �����������.
  * 3. ��� ������ �� ����� ��������������, ����� ����������� ��� ����������
  *    ��������, ���������� � �������������� ����� ������������ �����������
  *    ��� ������������ �� �� ����������.
  *
  * ��� ����������� ����������� ��������������� ������� ``��� ����'', ���
  * �����-���� ����� ����������� ��� ��������������� ��������, �������,
  * �� �� ������������� ����, �������� �������� ������ � �����������
  * ��� ���������� ����. ����� �� ��� ����� �������� �� ������������
  * �� ����� ������ - ������, ���������, ���������, �����������, ����������
  * ��� ���������������� (�������, �� �� ������������� ����, ����������
  * ������ ������ ��� �������; ������ �������������, ������ ��� ������;
  * ��� ����������� �������), ������ ��������� �� ����� ������ ���������������,
  * ���� � ���������, ������ ���������������, ���� � ��������� �������� ����
  * (������� ��� ��� ����� �����������), ����������� ������ �� �������������
  * ����� ������������ �����������, ���� ���� ���� �������������� � �����������
  * ������ �����������.
  *
  * ���� ���� �������� ������ ����� uIP TCP/IP.
  *
  * Author: Adam Dunkels <adam@sics.se>
  *
  * $Id: pt.h,v 1.2 26/06/12 08::30 adam Exp $
  */
 
 /**
  * \addtogroup pt
  * @{
  */
 
 /**
  * \file
  * ���������� protothread-��.
  * \author
  * Adam Dunkels <adam@sics.se>
  *
  */
 
 #ifndef __PT_H__
 #define __PT_H__
 
 #include "lc.h"
 
 typedef struct {
   lc_t lc;
 }pt_t;
 
 #define PT_WAITING 0
 #define PT_EXITED  1
 #define PT_ENDED   2
 #define PT_YIELDED 3
 
 /**
  * \name Initialization
  * @{
  */
 
 /**
  * ������������� protothread.
  *
  * �������������� protothread. ������������� ������ ���� ��������� �� ������
  * ���������� protothread.
  *
  * \param pt ��������� �� protothread control structure (��������� ����������).
  *
  * \sa PT_SPAWN()
  *
  * \hideinitializer
  */
 #define PT_INIT(pt)   LC_INIT((pt)->lc)
 
 /** @} */
 
 /**
  * \name ���������� � �����������
 * @{
  */
 
 /**
  * ���������� protothread.
  *
  * ���� ������ ������������ ��� ���������� protothread. ��� protothread-� ������
  * ���� ��������������� � ������� ����� �������.
  *
  * \param name_args ��� � ��������� ������� C, ����������� protothread.
  *
  * \hideinitializer
  */
#define PT_THREAD(name_args) char name_args
 
 /**
  * ����������� ������ protothread ������ ������� C, ����������� protothread.
  *
  * ���� ������ ������������ ��� �������������� ��������� ����� ���
  * protothread. ��� ������ ���� ��������� � ������ �������, � �������
  * ����������� protothread. ��� ��������� C �� ������ PT_BEGIN()
  * ����� ��������� ������ ���, ����� ����������� �������� ��� protothread.
  *
  * \param pt ��������� �� protothread control structure.
  *
  * \hideinitializer
  */
 #define PT_BEGIN(pt) { volatile char PT_YIELD_FLAG = 1; LC_RESUME((pt)->lc)
 
 /**
  * ������������� ����� ��� protothread.
  *
  * ���� ������ ������������ ��� �������������� �����, ��� ������������� 
  * protothread. �� ������ ������ �������������� ��������� � ��������������� 
  * ������ ������� ������� PT_BEGIN().
  *
  * \param pt ��������� �� protothread control structure.
  *
  * \hideinitializer
  */
 #define PT_END(pt) LC_END((pt)->lc); PT_YIELD_FLAG = 0; \
                    PT_INIT(pt); return PT_ENDED; }
 
 /** @} */
 
 /**
  * \name ����������� ��������.
  * @{
  */
 
 /**
  * ��������� ���������� � ����, ���� ������� �� ������ true.
  *
  * ���� ������ ��������� ���������� protothread, ���� ��������� ������� ��
  * ������ ������ true.
  *
  * \param pt ��������� �� protothread control structure.
  * \param condition �������.
  *
  * \hideinitializer
  */
 #define PT_WAIT_UNTIL(pt, condition)            \
   do {                                          \
     LC_SET((pt)->lc);                           \
     if(!(condition)) {                          \
       return PT_WAITING;                        \
     }                                           \
   } while(0)
 
 /**
  * ���������� � ��������, ���� ������� true.
  *
  * ��� ������� ��������� ���������� � ����, ���� ������� � ��������� true.
  * ��. ����� PT_WAIT_UNTIL().
  *
  * \param pt ��������� �� protothread control structure.
  * \param cond �������.
  *
  * \hideinitializer
  */
 #define PT_WAIT_WHILE(pt, cond)  PT_WAIT_UNTIL((pt), !(cond))
 
 /** @} */
 
 /**
  * \name �������� protothread-��
  * @{
  */
 
 /**
  * ��������� � ����, ���� �� ���������� �������� protothread.
  *
  * ���� ������ ��������� �������� ��������� protothread. ������� protothread
  * ����� ������������, ���� �������� protothread �� ����������.
  *
  * \note �������� protothread ������ ���� ��������������� ������� ��������
  * PT_INIT() �� ����, ��� ����� ������������ ������� PT_SPAWN.
  *
  * \param pt ��������� �� protothread control structure.
  * \param thread �������� protothread � �����������.
  *
  * \sa PT_SPAWN()
  *
  * \hideinitializer
  */
 #define PT_WAIT_THREAD(pt, thread) PT_WAIT_WHILE((pt), PT_SCHEDULE(thread))
 
 /**
  * ��������� �������� protothread � ���� ������ �� ����.
  *
  * ���� ������ ��������� �������� protothread � ����, ���� �� �� ��������
  * �����. ������ ����� ���� ����������� ������ � �������� protothread.
  * macro can only be used within a protothread.
  *
  * \param pt ��������� �� protothread control structure.
  * \param child ��������� �� protothread control structure ��������� protothread.
  * \param thread �������� protothread � �����������.
  *
  * \hideinitializer
  */
 #define PT_SPAWN(pt, child, thread)             \
   do {                                          \
     PT_INIT((child));                           \
     PT_WAIT_THREAD((pt), (thread));             \
   } while(0)
 
 /** @} */
 
 /**
  * \name ����� � �������
  * @{
  */
 
 /**
  * ������ ������� ��� protothread.
  *
  * ���� ������ ����������� � ������������ ���������� protothread
  * � ���� �����, ��� ����� ����� PT_BEGIN().
  *
  * \param pt ��������� �� protothread control structure.
  *
  * \hideinitializer
  */
 #define PT_RESTART(pt)                          \
   do {                                          \
     PT_INIT(pt);                                \
     return PT_WAITING;                  \
   } while(0)
 
 /**
  * ����� �� protothread.
  *
  * ���� ������ ���������� ����� �� protothread. ���� protothread ���
  * �������� ������ protothread, �� ������������ protothread ������
  * ���������������� � ������ ���������� ������.
  *
  * \param pt ��������� �� protothread control structure.
  *
  * \hideinitializer
  */
 #define PT_EXIT(pt)                             \
   do {                                          \
     PT_INIT(pt);                                \
     return PT_EXITED;                   \
   } while(0)
 
 /** @} */
 
 /**
  * \name ����� protothread
  * @{
  */
 
 /**
  * �������� ��� protothread.
  *
  * ��� ������� ������������ �������� ��� protothread. ������������ ��������
  * ������� �� ����� 0, ���� protothread ��������, ��� 0, ���� protothread 
  * ������ �����.
  *
  * \param f ����� C-�������, ������� ��������� �������� ��� protothread.
  *
  * \hideinitializer
  */
 #define PT_SCHEDULE(f) ((f) == PT_WAITING)
 
 /** @} */
 
 /**
  * \name ������� ��������� (Yielding) �� protothread
  * @{
  */
 
 /**
  * ������� ��������� ���������� (Yield) �� �������� protothread.
  *
  * ��� ������� �������� ������������ ����� ����� protothread, ��� ���������
  * ������ protothread ����������� � �������.
  *
  * \param pt ��������� �� protothread control structure.
  *
  * \hideinitializer
  */
 #define PT_YIELD(pt)                            \
   do {                                          \
     PT_YIELD_FLAG = 0;                          \
     LC_SET((pt)->lc);                           \
     if(PT_YIELD_FLAG == 0) {                    \
       return PT_YIELDED;                        \
     }                                           \
   } while(0)
 
/**
  * \brief      ������ ������� ������������� ������� �� protothread, ���� ��
  *             �� ����� ��������� �������.
  * \param pt   ��������� �� protothread control structure.
  * \param cond �������.
  *
  *             ��� ������� ����� ��������� � ������� ������������� ������� 
  *             ��� ����� protothread, ���� ��������� ������� �����������
  *             ��� true.
  *
  *
  * \hideinitializer
  */
 #define PT_YIELD_UNTIL(pt, cond)                \
   do {                                          \
     PT_YIELD_FLAG = 0;                          \
     LC_SET((pt)->lc);                           \
     if((PT_YIELD_FLAG == 0) || !(cond)) {       \
       return PT_YIELDED;                        \
     }                                           \
   } while(0)
 
 /** @} */
 
 #endif /* __PT_H__ */
 
 /** @} */