
/*
 * Copyright (c) 2004, Swedish Institute of Computer Science.
 * All rights reserved. 
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met: 
 * 1. Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 3. Neither the name of the Institute nor the names of its contributors 
 *    may be used to endorse or promote products derived from this software 
 *    without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE. 
 *
 * This file is part of the protothreads library.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 * $Id: pt-sem.h,v 1.2 2005/02/24 10:36:59 adam Exp $
 */

/**
 * \addtogroup pt
 * @{
 */

/**
 * \defgroup ptsem Protothread semaphores
 * @{
 *
 * ���� ������ ���������� ������� �������� ��� ������������. �������� 
 * �������� ��� �������� ������������� ������� ������������ ��� ��������:
 * : "�����" � "������". �������� "�����" ��������� ������� ��������
 * � ��������� ����� ���� ������� ����� ����. �������� "������" �����������  
 * ������� �������� �� �� ��������� ���. ���� ������ ����� ���������� 
 * ��������� �������� ������� �����, �� �� ��������������.
 *
 * �������� ����� ���� ������������ ����� ��������� ������, ����� ����������������� ,
 * ��������� ������������� ������ ��� �������� � ��������� 
 * queues/bounded buffers (������ ����).
 *
 * ��������� ������ ���������� ��� ������������� - �����������, ����� 
 * ��������� ��� �������� bounded buffer problem, ����� ���� ������ ����� ������������� 
 * ������������ � ���������. ������� �� �������� ������� ����� ������� 
 *
 \code
#include "pt-sem.h"

#define NUM_ITEMS 32
#define BUFSIZE 8

static struct pt_sem mutex, full, empty;

PT_THREAD(producer(struct pt *pt))
{
  static int produced;
  
  PT_BEGIN(pt);
  
  for(produced = 0; produced < NUM_ITEMS; ++produced) {
  
    PT_SEM_WAIT(pt, &full);
    
    PT_SEM_WAIT(pt, &mutex);
    add_to_buffer(produce_item());    
    PT_SEM_SIGNAL(pt, &mutex);
    
    PT_SEM_SIGNAL(pt, &empty);
  }

  PT_END(pt);
}

PT_THREAD(consumer(struct pt *pt))
{
  static int consumed;
  
  PT_BEGIN(pt);

  for(consumed = 0; consumed < NUM_ITEMS; ++consumed) {
    
    PT_SEM_WAIT(pt, &empty);
    
    PT_SEM_WAIT(pt, &mutex);    
    consume_item(get_from_buffer());    
    PT_SEM_SIGNAL(pt, &mutex);
    
    PT_SEM_SIGNAL(pt, &full);
  }

  PT_END(pt);
}

PT_THREAD(driver_thread(struct pt *pt))
{
  static struct pt pt_producer, pt_consumer;

  PT_BEGIN(pt);
  
  PT_SEM_INIT(&empty, 0);
  PT_SEM_INIT(&full, BUFSIZE);
  PT_SEM_INIT(&mutex, 1);

  PT_INIT(&pt_producer);
  PT_INIT(&pt_consumer);

  PT_WAIT_THREAD(pt, producer(&pt_producer) &
		     consumer(&pt_consumer));

  PT_END(pt);
}
 \endcode
 *
 * ��� ��������� ���������� ��� �����������: ���� ���������� ������� 
 * ��������� ������� �����������, ���� ����� ������� ��������� ������� ,
 * ������������� � ���� ����� ������� ��������� ����� ������� �������������.
 * ��������� ���������� ��� ��������: "������", "������" � "�������". 
 * ������� "�������" ������������ ����� ���������� ����� ���������� ��� 
 * �������, "������" ������� ������������ ����� ����������� ����������� ���� 
 * ������ ����, � "������" ������� ������������ ����� ����������� 
 * ������������� ���� ������ �����.
 *
 * ���������� "driver_thread" ������ ��� ���������� - ��������� �����������,
 * "pt_producer" � "pt_consumer". ����� �������� ��� ��� ��� ���������� 
 * ������� ��� <i>static</i>. ���� �������� ����� "static" �� ������������,
 * ��� ���������� ����������� � �����. ��� ��� ����������� �� ����������� � �����,
 * ��� ���������� ����� ���� ������������ � ������� ���������� �������� ������������
 * ��������. ����������, ��� "�����������" � "�������������" ��������� ���������
 * ���������� ��� static, ����� �������� ���������� �� � ����� ���������.
 * 
 *
 */
   
/**
 * \file
 * ������� �������� ��������� �� ������������
 * \author
 * Adam Dunkels <adam@sics.se>
 *
 */

#ifndef __PT_SEM_H__
#define __PT_SEM_H__

#include "pt.h"

typedef struct {
  unsigned int count;
}pt_sem_t;

/**
 * ������������� ��������
 *
 * ���� ������ �������������� �������. ���� ������ �������������� ������� � �������� 
 * counter. ������� ��������������� ���������� "unsigned int" ����� 
 * represent the counter, � ������������� �������� "count" ������ ���� 
 * � ��������� unsigned int.
 *
 * \param s (struct pt_sem *) ��������� �� ��������� pt_sem struct
 * �������������� ������� 
 *
 * \param c (unsigned int) ��������� �������� �������� ��������.
 * \hideinitializer
 */
#define PT_SEM_INIT(s, c) (s)->count = c

/**
 * ���� �������
 *
 * ���� ������ ��������� �������� "��������" ��������. �������� 
 * �������� ��������� ���������� ���� ������� �� ������ ����� ����
 * ����� ������� ��������� �������� ��������, ��� ����,
 * ���������� ��������� ����������� .
 *
 * \param pt (struct pt *) ��������� �� ���������� (struct pt) � 
 * ������� ����������� ��������.
 *
 * \param s (struct pt_sem *) ��������� �� ��������� pt_sem struct
 * ��������������� ��������
 *
 * \hideinitializer
 */
#define PT_SEM_WAIT(pt, s)	\
  do {						\
    PT_WAIT_UNTIL(pt, (s)->count > 0);		\
    --(s)->count;				\
  } while(0)

/**
 * ������ �������� 
 *
 * ���� ������ ������ �������. ������ �������� �������������� ������� 
 * ������ ��������, ������� � �������� ����� ����� ��������
 * ����������� ���������� �����������.
 *
 * \param pt (struct pt *) ��������� �� ���������� (struct pt) � 
 * ������� ����������� ��������.
 *
 * \param s (struct pt_sem *) ��������� �� ��������� pt_sem struct
 * ��������������� ��������
 *
 * \hideinitializer
 */
#define PT_SEM_SIGNAL(pt, s) ++(s)->count

#endif /* __PT_SEM_H__ */

/** @} */
/** @} */
   
